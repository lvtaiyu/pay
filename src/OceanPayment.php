<?php
/**
 * Created by : PhpStorm
 * User: lty
 * Time: 2023/3/28
 */


namespace Tty199\Pay;

use Tty199\Pay\Contract\OceanPayInterface;
use Tty199\Pay\Contract\OrderPayInterface;

class OceanPayment implements OceanPayInterface
{

    public function payResult(OrderPayInterface $order)
    {
        echo "this is ocean payment" . PHP_EOL;
    }

}