<?php
/**
 * Created by : PhpStorm
 * User: lty
 * Time: 2023/3/28
 */


namespace Tty199\Pay;


use Tty199\Pay\Contract\OrderPayInterface;
use Tty199\Pay\Contract\PaypalInterface;

class Paypal implements PaypalInterface
{

    public function payResult(OrderPayInterface $order)
    {
        echo "this is paypal" . PHP_EOL;
    }

}