<?php
/**
 * Created by : PhpStorm
 * User: lty
 * Time: 上午11:35
 */


namespace Tty199\Pay\Contract;


interface PayStrategyInterface
{
    public function payResult(OrderPayInterface $order);
}