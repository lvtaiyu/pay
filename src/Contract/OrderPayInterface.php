<?php
/**
 * Created by : PhpStorm
 * User: lty
 * Time: 上午11:56
 */


namespace Tty199\Pay\Contract;


interface OrderPayInterface
{

    /**
     * 获取订单的真实付款价格
     * 可能各种优惠的叠加导致显示给用户和真实支付的价格不同因此我们需要获取订单的真实支付价格
     *
     * @return float
     * @author lty
     */
    public function getActuallyPrice(): float;

    public function getCurrency(): string;

    /**
     * 流水号非订单号
     *
     * @return string
     * @author lty
     */
    public function getTransactionNumber(): string;

    public function getPayType(): string;
}