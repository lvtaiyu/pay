<?php
/**
 * Created by : PhpStorm
 * User: lty
 * Time: 2023/3/28
 */


namespace Tty199\Pay;


use InvalidArgumentException;
use Tty199\Pay\Contract\OrderPayInterface;

class PayWayFactory
{
   private static $payWay = [
       'ocean_payment' => OceanPayment::class,
       'paypal' => Paypal::class,
   ];

   public function pay(OrderPayInterface $order): bool
   {
       // TODO 策略模式差不多就这样了，将具体的支付逻辑（算法）封装并且通过订单支付类型选定支付算法
       // TODO 这里可能还要做的就是支持支付前后的事件以及失败掉或者成功了的事件吧。暂无具体参考。可能还可以安排成一个具体的服务呢
       if (empty(self::$payWay[$order->getPayType()])) {
           throw new InvalidArgumentException("unsupported payment types: {$order->getPayType()}");
       }

       /**
        * @var \Tty199\Pay\Contract\PayStrategyInterface $strategy
        */
       $strategy = new self::$payWay[$order->getPayType()];
       $strategy->payResult($order);

       echo "支付结束 after payment event ... " . PHP_EOL;
       return true;
   }
}