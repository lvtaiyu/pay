<?php
/**
 * Created by : PhpStorm
 * User: lty
 * Time: 2023/3/28
 */


namespace Tty199\Pay;


use Tty199\Pay\Contract\OrderPayInterface;

class OrderTest implements OrderPayInterface
{

    public function getActuallyPrice(): float
    {
        return 111.1;
    }

    public function getCurrency(): string
    {
        return 'USD';
    }

    public function getTransactionNumber(): string
    {
        return "test_num_" . time();
    }

    public function getPayType(): string
    {
        return 'paypal';
    }

}